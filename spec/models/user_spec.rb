# -*- encoding : utf-8 -*-
require 'rails_helper'

describe User do

  before(:each) do
    @user = create(:user)
    @admin = create(:admin)
  end

  it "should admin has flag admin" do
    expect(@admin.admin?).to eq(true)
  end

  it "should user has not flag admin" do
    expect(@user.admin?).to eq(false)
  end

end
