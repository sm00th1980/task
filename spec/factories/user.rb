# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :user do
    sequence(:email) { |n| "email_#{n}@mail.ru" }
    password '12345678'
    password_confirmation '12345678'
    admin false

    factory :admin do
      admin true
    end

    # factory :client do
    #   admin false
    #
    #   after(:create) do |user|
    #     FactoryGirl.create(:terminal, user: user)
    #   end
    # end
  end
end
