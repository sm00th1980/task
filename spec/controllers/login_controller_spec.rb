# -*- encoding : utf-8 -*-
require 'rails_helper'

describe LoginController do
  describe "GET index" do
    render_views

    before(:each) do
      @user = create(:user)
      @admin = create(:admin)
    end

    it "should redirect to new_session if non-auth user" do
      get :index
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should redirect to task index page for user" do
      sign_in @user
      get :index
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(tasks_path)
    end

    it "should redirect to admin index page for admin" do
      sign_in @admin
      get :index
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(admin_path)
    end
  end
end
