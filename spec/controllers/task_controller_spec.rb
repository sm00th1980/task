# -*- encoding : utf-8 -*-
require 'rails_helper'

describe TaskController do
  render_views

  before(:each) do
    @user = create(:user)
  end

  describe "GET tasks" do
    it "should redirect to new_session if non-auth user" do
      get :index
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should show tasks page for client" do
      sign_in @user
      get :index
      expect(response).to have_http_status(:success)
    end
  end

end
