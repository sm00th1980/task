# -*- encoding : utf-8 -*-
require 'rails_helper'

describe AdminController do
  render_views

  before(:each) do
    @admin = create(:admin)
  end

  describe "GET index" do
    it "should redirect to new_session for non-auth user" do
      get :index
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should show users page for admin" do
      sign_in @admin
      get :index
      expect(response).to have_http_status(:success)
    end
  end

end
