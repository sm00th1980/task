# -*- encoding : utf-8 -*-
class AddAdminColumnIntoUsers < ActiveRecord::Migration
  def change
    add_column :users, :admin, :boolean, null: false, default: false
  end
end
