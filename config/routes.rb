# -*- encoding : utf-8 -*-
Rails.application.routes.draw do
  devise_for :users, :path => "/", :path_names => {:sign_in => 'login', :sign_out => 'logout'}, :controllers => {:sessions => "sessions"}

  get 'admin' => 'admin#index', as: :admin
  get 'tasks' => 'task#index', as: :tasks

  root 'login#index'
end
