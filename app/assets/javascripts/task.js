(function ($) {
    $(document).ready(function () {
        $("#sortable1, #sortable2, #sortable3, #sortable4, #sortable5").sortable({
            connectWith: ".connectedSortable",
            stop: function (event, ui) {
                var task_id = ui.item[0].id;
                var new_status_id = this.id;
                var list = $(this);

                $.ajax({
                    url: "test.html",
                    data: {
                        task_id: task_id,
                        new_status_id: new_status_id
                    }
                }).done(function() {
                    //success server response
                    

                }).fail(function() {
                    //failure -> cancel all sorting
                    list.sortable("cancel");
                });
            }
        }).disableSelection();
    });
}(jQuery));