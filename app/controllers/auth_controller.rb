# -*- encoding : utf-8 -*-
class AuthController < ApplicationController
  before_action :authenticate_user!

  def authenticate_user!
    if current_user.nil?
      sign_out current_user if current_user.present?
      flash[:alert] = t('authorization_is_required')
      redirect_to new_user_session_path
    end
  end
end
