# -*- encoding : utf-8 -*-
class LoginController < NonAuthController
  def index
    if user_signed_in? and current_user
      if current_user.admin?
        redirect_to admin_path
      else
        redirect_to tasks_path
      end
    else
      redirect_to new_user_session_path
    end
  end
end
