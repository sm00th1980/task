# -*- encoding : utf-8 -*-
class SessionsController < Devise::SessionsController
  def create
    username, password = get_username_password

    if auth_user?(username, password)
      flash[:notice] = t('login')

      sign_in @user
    else
      #non auth
      flash[:alert] = t('authorization_is_required')

      redirect_to new_user_session_path and return
    end

    redirect_to root_path
  end

  def destroy
    flash[:notice] = t('logout')
    sign_out current_user

    redirect_to root_path
  end

  private
  def get_username_password
    [sanitize_params[:email], sanitize_params[:password]]
  end

  def user_exist?(username)
    User.exists?(:email => username)
  end

  def auth_user?(username, password)
    user = User.find_by_email(username)
    if user and user.valid_password?(password)
      @user = user
      return true
    end

    false
  end

  def sanitize_params
    params.permit(:email, :password, :authenticity_token)
  end
end
